import { Field } from "formik";
import css from '../../styles/InputFile.module.scss'

function InputFile({ filepath, onClear }) {
  if(filepath) {
    let filename = filepath.replace(/.+\\(.+)/g,"$1");
    return (
      <div className={css.block_uploaded}>
        <div className={css.block_uploaded__image}></div>
        <span className={css.block_uploaded__name}>{filename}</span>
        <a className={css.block_uploaded__close} onClick={ onClear }></a>
      </div>
    )
  } else {
    return (
      <label className={css.block_file}>
        <div className={css.block_file__image}></div>
        <span>Загрузить резюме</span>
        <Field type="file" name="portfolio" />
      </label>
    );
  }
}

export default InputFile;