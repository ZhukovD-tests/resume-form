import { Field } from "formik";
import css from '../../styles/ResumeForm.module.scss'

function Input ({ title, name, placeholder, type, errors }) {
  return (
    <label>
      <p className={css.input_title}>{title}</p>
      <Field className={(errors[name])?css.input_error:""} type={(type)?type:"text"} name={name} placeholder={placeholder}/>
      <span className={css.input__error_message}>{errors[name]}</span>
    </label>
  )
}
export default Input;