import { Field } from "formik";
import Radio from './../Markers/Radio/Radio'

function InputRadio ({ title, name, value, values }) {
  return (
    <label>
      <Radio checked={values[name] === value} />
      <Field type="radio" name={name} value={value} />
      <span>{title}</span>
    </label>
  )
}
export default InputRadio;