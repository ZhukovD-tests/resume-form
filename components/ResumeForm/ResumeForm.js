
import { useState } from "react";
import { Formik, Form } from "formik";
import css from '../../styles/ResumeForm.module.scss'
import * as yup from "yup";
import FormPopup from "./../FormPopup/FormPopup";
import AgreementPopup from "./../AgreementPopup/AgreementPopup";
import Fade from "./../Fade/Fade";
import Input from "./../Input/Input";
import InputFile from "./../InputFile/InputFile";
import InputRadio from "./../InputRadio/InputRadio";
import InputAgreement from './../InputAgreement/InputAgreement';

function ResumeForm() {

  const [showPopupName, setShowPopupName] = useState("");
  const [showPopupAgreement, setShowPopupAgreement] = useState(false);

  const initialValues = {
    email: "",
    name: "",
    surname: "",
    portfolio: "",
    gender: "",
    github: "",
    agreement: false
  };
  const validationSchema = yup.object().shape({
    name: yup.string()
      .required("Введите имя")
      .matches(
        /^[A-Za-z\u0401\u0451\u0410-\u044f]+$/,
        {
          message: 'В имени могут быть только буквы',
          excludeEmptyString: true,
        },
      ),
    surname: yup.string()
      .required("Введите фамилию")
      .matches(
        /^[A-Za-z\u0401\u0451\u0410-\u044f\-\ ]+$/,
        {
          message: 'В фамилии могут быть только буквы',
          excludeEmptyString: true,
        },
      ),
    email: yup.string()
      .email('E-mail недействителен')
      .required('Пожалуйста, укажите электронную почту'),
    gender: yup.string().required("укажите пол"),
    github: yup.string(),
    agreement: yup.boolean()
      .required("Cогласитесь с политикой конфидециальности")
      .oneOf([true], "Cогласитесь с политикой конфидециальности"),
    portfolio: yup
      .mixed()
  });

  return (
    <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        validateOnBlur={false}
        validateOnChange={false}
        onSubmit={(values, actions) => {
          setShowPopupName(values.name);
          actions.setSubmitting(false);
        }}
    >
      {({
        values,
        errors,
        handleSubmit,
        setFieldValue,
        resetForm,
      }) => {
          
        const handleAgreement = (result) => {
          console.log(result);
          setFieldValue("agreement", result, false); 
          setShowPopupAgreement(false)
        }

        const isFormPopup = (formReset) => {
          return (
            <Fade show={showPopupName}>
              <FormPopup username={showPopupName} onClose={ () => {setShowPopupName(""); formReset() } }/>
            </Fade>
          )
        }
        
        const isAgreementPopup = () => {
          return (
            <Fade show={showPopupAgreement}>
              <AgreementPopup onClose={ handleAgreement }/>
            </Fade>
          )
        }
        return (
          <main className={css.main}>
            {isFormPopup( resetForm )}
            {isAgreementPopup()}
            <h1 className={css.title}>
              Анекта соискателя
            </h1>
            <Form className={css.cvform} onSubmit={handleSubmit}>
              <h3>Личные данные</h3>
              <div className={css.cvform__block + " " +  css.block__personal}>
                <Input name="name" title="Имя *" placeholder="Имя" errors={errors} />
                <Input name="surname" title="Фамилия *" placeholder="Фамилия" errors={errors} />
                <Input name="email" title="Электронная почта *" name="email" placeholder="Электронная почта" errors={errors} />
                <InputFile filepath={values.portfolio} onClear={() => { setFieldValue("portfolio", "", false) }} />
              </div>

              <h3 className={css.input_title}>
                Пол * 
                <span className={css.input__error_message}>{errors.gender}</span> 
              </h3>
              <div className={css.cvform__block + " " +  css.block__gender}>
                <InputRadio title="Мужской" name="gender" value="male" values={values} />
                <InputRadio title="Женский" name="gender" value="female" values={values} />
              </div>         

              <h3 className={css.input_title}>
                <span className={css.input_title__name}>Github</span>
              </h3> 
              <div className={css.cvform__block + " " +  css.block__github}>
                <Input name="github" title="Введите ссылку на Github" placeholder="Вставьте ссылку на Github" errors={errors} />
              </div>
              
              <div className={css.cvform__block + " " +  css.block__agreement}>
                <InputAgreement name="agreement" values={values} errors={errors} onClick={()=>{ setShowPopupAgreement(true) }}/>
              </div>
              <input type="submit" />
            </Form>  
          </main>
        );
      }}
    </Formik>  
  )
}

export default ResumeForm;