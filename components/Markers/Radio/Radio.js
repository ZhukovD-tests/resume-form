import classnames from "classnames";
import css from '../../../styles/Radio.module.scss'

function Radio ({ checked }) {
  return (
    <div className={classnames(css.radio, {[css.checked]: checked}) }>
      <span className={css.radio__custom}></span>
    </div>
  )
}

export default Radio;