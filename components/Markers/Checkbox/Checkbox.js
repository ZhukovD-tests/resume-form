import classnames from "classnames";
import css from '../../../styles/Checkbox.module.scss'

function Checkbox ({ checked }) {
  return (
    <div className={classnames(css.checkbox, {[css.checked]: checked}) }>
      <span className={css.checkbox__custom}></span>
    </div>
  )
}

export default Checkbox;