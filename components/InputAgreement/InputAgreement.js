import { Field } from "formik";
import css from '../../styles/ResumeForm.module.scss'
import Checkbox from './../Markers/Checkbox/Checkbox'

function InputAgreement ({ name, values, errors, onClick }) {
  return (
    <label>
      <span className={css.input__error_message}>{errors[name]}</span>
      <div className={css.input_wrapper}>
        <Checkbox checked={values[name]}/>
        <Field type="checkbox" name={name}/>
        <span className={css.input_wrapper__text}>
        * Я согласен с <a onClick={onClick}> политикой конфиденциальности</a>
        </span>
      </div>
    </label>
  )
}
export default InputAgreement;