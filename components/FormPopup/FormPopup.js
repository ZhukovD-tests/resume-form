import css from '../../styles/FormPopup.module.scss'

function FormPopup({ username, onClose }) {
  if(username) {
    return (
      <div className={css.form_popup_back}>
        <div className={css.form_popup}>
          <h2 className={css.form_popup__username}>Спасибо, {username}!</h2>
          <p className={css.form_popup__text}>Мы скоро свяжемся с вами</p>
          <button className={css.form_popup__close_btn} onClick={ onClose }>Понятно</button>
        </div>
      </div>
    );
  } else {
    return null;
  }
}

export default FormPopup;