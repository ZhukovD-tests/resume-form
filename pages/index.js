import Head from 'next/head'
import css from '../styles/Home.module.scss'
import ResumeForm from './../components/ResumeForm/ResumeForm';

function Home() {
  return (
    <div className={css.container}>
      <Head>
        <title>Resume Form</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ResumeForm />
  </div>
  )
}

export default Home;